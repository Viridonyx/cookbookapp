package com.example.cookbook;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openChicken(View view){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://tasty.co/recipe/creamy-tuscan-chicken"));
        startActivity(i);
    }

    public void openRatatouille(View view){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://tasty.co/recipe/ratatouille"));
        startActivity(i);
    }

    public void openSalmon(View view){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://tasty.co/recipe/honey-soy-glazed-salmon"));
        startActivity(i);
    }

    public void openStirFry(View view){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://tasty.co/recipe/chicken-veggie-stir-fry"));
        startActivity(i);
    }

    public void openSoup(View view){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://tasty.co/recipe/classic-chicken-noodle-soup"));
        startActivity(i);
    }

    public void openMeatball(View view){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://tasty.co/recipe/one-pot-swedish-meatball-pasta"));
        startActivity(i);
    }
}